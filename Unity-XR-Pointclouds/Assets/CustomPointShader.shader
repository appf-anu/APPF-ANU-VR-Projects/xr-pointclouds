﻿// Pcx - Point cloud importer & renderer for Unity
// https://github.com/keijiro/Pcx

Shader "Point Cloud/CustomPointShader"
{
    Properties
    {
        _Tint("Tint", Color) = (0.5, 0.5, 0.5, 1)
        _PointSize("Point Size", Float) = 0.05
        [Toggle] _Distance("Apply Distance", Float) = 1
    }
        SubShader
    {
        Tags { "RenderType" = "Opaque" }
        Pass
        {
            CGPROGRAM

            #pragma vertex Vertex
            #pragma fragment Fragment

            #pragma multi_compile_fog
            #pragma multi_compile _ UNITY_COLORSPACE_GAMMA
            #pragma multi_compile _ _DISTANCE_ON

            #include "UnityCG.cginc"
            #include "Common.cginc"

            struct Attributes
            {
                float4 position : POSITION;
                half3 color : COLOR;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct Varyings
            {
                float4 position : SV_Position;
                half3 color : COLOR;
                half psize : PSIZE;
                UNITY_FOG_COORDS(0)
                UNITY_VERTEX_OUTPUT_STEREO
            };

            half4 _Tint;
            float4x4 _Transform;
            half _PointSize;




            Varyings Vertex(Attributes input)
            {
                Varyings o;

                UNITY_SETUP_INSTANCE_ID(input);

                    UNITY_INITIALIZE_OUTPUT(Varyings, o);

                    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                float4 pos = input.position;
                half3 col = input.color;

                //// Dimention of geometry
                //float2 dim = float2(_PointSize, _PointSize);

                //// Create equilateral triangle
                //float2 p[3];
                //p[0] = float2(-dim.x, dim.y * .57735026919);
                //p[1] = float2(0., -dim.y * 1.15470053838);
                //p[2] = float2(dim.x, dim.y * .57735026919);

                //// Update geometry
                //[unroll]
                //for (int idx = 0; idx < 3; idx++) {
                //    p[idx] = mul(1, p[idx]);    // apply rotation
                //    //p[idx].x *= _ScreenParams.y / _ScreenParams.x; // make square
                //    o.position = float4(p[idx], 0, 0) / 2.;
                //    //UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                //    //triangleStream.Append(o);
                //}

            #ifdef UNITY_COLORSPACE_GAMMA
                col *= _Tint.rgb * 2;
            #else
                col *= LinearToGammaSpace(_Tint.rgb) * 2;
                col = GammaToLinearSpace(col);
            #endif


                o.position = UnityObjectToClipPos(pos);
                o.color = col;
            #ifdef _DISTANCE_ON
                o.psize = _PointSize / o.position.w * _ScreenParams.y;
            #else
                o.psize = _PointSize;
            #endif
                UNITY_TRANSFER_FOG(o, o.position);
                return o;
            }

            half4 Fragment(Varyings input) : SV_Target
            {
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input); //Insert

                half4 c = half4(input.color, _Tint.a);
                UNITY_APPLY_FOG(input.fogCoord, c);
                return c;
            }

            ENDCG
        }
    }
        CustomEditor "Pcx.PointMaterialInspector"
}
