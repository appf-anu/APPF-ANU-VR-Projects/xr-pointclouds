﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepositionPointCloud : MonoBehaviour
{
    [ContextMenu("Reposition Mesh to Centre")]
    public void RepositionMeshToCentre()
    {
        Vector3 MeshCentre = transform.GetComponent<MeshRenderer>().bounds.center;

        transform.Translate(-MeshCentre);
    }
}
