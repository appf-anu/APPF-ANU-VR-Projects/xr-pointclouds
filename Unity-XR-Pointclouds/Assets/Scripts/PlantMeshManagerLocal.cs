﻿using Microsoft.MixedReality.Toolkit.Experimental.UI;
using Microsoft.MixedReality.Toolkit.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;

/// <summary>
/// Class to manage the Plant Meshes of a single Plant Block obtained from a Local Instance
/// </summary>
public class PlantMeshManagerLocal : MonoBehaviour
{

    [SerializeField] private List<Mesh> plantMeshes;
    private List<PointcloudJSON> plantDataList = new List<PointcloudJSON>();
    private int currentMeshIndex = 0;

    [SerializeField] private MeshFilter plantMeshFilter;
    [SerializeField] private MeshRenderer plantMeshRenderer;


    [SerializeField] StepSlider sliderForMeshes;

    [SerializeField] TextAsset attributesCSV;

    List<Dictionary<string, object>> attributesListDict;
    [SerializeField] ContentTextHandler contentTextHandler;

    [SerializeField] TextMeshPro timestampText;
    [SerializeField] TextMeshPro titleText;

    private string timeFormatSpecifier = "g";
    private CultureInfo timeFormatCulture = new CultureInfo("ja-JP");



    private void OnEnable()
    {

        if (plantMeshes.Count <= 1)
        {
            sliderForMeshes.gameObject.SetActive(false);
        } else
        {
            sliderForMeshes.SliderStepDivisions = plantMeshes.Count - 1;
        }

        attributesListDict = CSVReader.Read(attributesCSV);


        // FIRST INSTANCE MESH
        ChangePlantMesh(0);

        object timestampObj;
        attributesListDict[0].TryGetValue("timestamp", out timestampObj);
        UpdateDateTimeText(timestampObj);

        //Change Title Text
        object titleObj;
        attributesListDict[0].TryGetValue("g_alias", out titleObj);
        titleText.text = titleObj.ToString();

        //Debug.Log("yeet " + attributesListDict[0].Keys.ForEach);

        //foreach (var x in attributesListDict[0].Keys)
        //{
        //    object val;
        //    attributesListDict[0].TryGetValue(x, out val);
        //    Debug.Log(x.ToString() + ": " + val.ToString());

        //}
    }

    public void OnSliderUpdated(SliderEventData eventData)
    {
        int newIndex = (int) (eventData.NewValue * (plantMeshes.Count - 1));

        Debug.Log(newIndex);
        ChangePlantMesh(newIndex);

        string res = "";
        foreach (var keys in attributesListDict[newIndex].Keys)
        {
            object val;
            attributesListDict[newIndex].TryGetValue(keys, out val);
            res += (keys.ToString() + ":\t" + val.ToString() + "\n");
            
            // ONly gor MVP Purposes
            if (res.Length > 300)
                break;
        }

        contentTextHandler.ChangeText(res);

        object timestampObj;
        attributesListDict[newIndex].TryGetValue("timestamp", out timestampObj);

        UpdateDateTimeText(timestampObj);
    }

    /// <summary>
    /// Updates the timetext
    /// </summary>
    /// <param name="time"></param>
    private void UpdateDateTimeText(object time)
    {

        DateTime timeStampDateTime = Convert.ToDateTime(time);
        timestampText.text = (timeStampDateTime.ToString(timeFormatSpecifier, timeFormatCulture));

        Debug.Log("UPDATE TEXT");
    }

    /// <summary>
    /// Changes the plant mesh based on the index
    /// </summary>
    /// <param name="index">index of plant mesh in plantMeshes array to be loaded</param>
    public void ChangePlantMesh(int index)
    {
        Mesh newMesh;
        try
        {
            newMesh = (Mesh) plantMeshes[index];
        }
        catch (Exception _)
        {
            Debug.Log("INDEX OUT OF BOUNDS - CANNOT CHANGE PLANT MESH");
            return;
        }

        currentMeshIndex = index;
        ChangePlantMesh(newMesh);
    }

    /// <summary>
    /// Changes the plant mesh based on a given mesh
    /// </summary>
    /// <param name="mesh">Mesh to change to</param>
    public void ChangePlantMesh(Mesh newMesh)
    {
        Debug.Log("Changing Plant Mesh");
        plantMeshFilter.sharedMesh = newMesh;
        CentrePlantMesh();
    }

    [ContextMenu("Centre Plant Mesh")]
    public void CentrePlantMesh()
    {

        //plantMeshRenderer.transform.localScale = Vector3.one * 3f;

        Vector3 MeshCentre = plantMeshRenderer.bounds.center;

        plantMeshRenderer.transform.Translate(-MeshCentre, Space.World);

        Vector3 parentPosition = plantMeshRenderer.transform.parent.position;

        plantMeshRenderer.transform.Translate(parentPosition, Space.World);
    }

    /// <summary>
    /// Decrease Plant Mesh Index to the previous plant Mesh 
    /// </summary>
    [ContextMenu("Decrease Plant Mesh")]
    public void DecreasePlantMesh()
    {
        if (currentMeshIndex <= 0)
            return;

        currentMeshIndex--;
        plantMeshFilter.sharedMesh = (Mesh) plantMeshes[currentMeshIndex];
    }

    /// <summary>
    /// Increases Plant Mesh Index to subsequent plant Mesh
    /// </summary>
    [ContextMenu("Increase Plant Mesh")]
    public void IncreasePlantMesh()
    {
        if (currentMeshIndex >= plantMeshes.Count-1)
            return;

        currentMeshIndex++;
        plantMeshFilter.sharedMesh = (Mesh) plantMeshes[currentMeshIndex];
    }
}
