﻿using Microsoft.MixedReality.Toolkit.Experimental.UI;
using Microsoft.MixedReality.Toolkit.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;

/// <summary>
/// Class to manage the Plant Meshes of a single Plant Block obtained from an Online 
/// </summary>
public class PlantMeshManager : MonoBehaviour
{

    [SerializeField] private List<Mesh> plantMeshes;
    private List<PointcloudJSON> plantDataList = new List<PointcloudJSON>();
    private int currentMeshIndex = 0;

    [SerializeField] public MeshFilter plantMeshFilter;
    [SerializeField] public MeshRenderer plantMeshRenderer;


    [SerializeField] StepSlider sliderForMeshes;

    [SerializeField] TextAsset attributesCSV;

    List<Dictionary<string, object>> attributesListDict;
    [SerializeField] ContentTextHandler contentTextHandler;

    [SerializeField] TextMeshPro timestampText;
    [SerializeField] TextMeshPro titleText;

    private string timeFormatSpecifier = "g";
    private CultureInfo timeFormatCulture = new CultureInfo("ja-JP");

    [SerializeField] TextMeshPro ErrorText;



    private void OnEnable()
    {

        attributesListDict = CSVReader.Read(attributesCSV);

        // Change Timestamp
        object timestampObj;
        attributesListDict[0].TryGetValue("timestamp", out timestampObj);
        UpdateDateTimeText(timestampObj);
    }

    /// <summary>
    /// Function called when there is a Slider Update function
    /// </summary>
    /// <param name="eventData">Event data from the Slider Update</param>
    public void OnSliderUpdated(SliderEventData eventData)
    {
        int newIndex = (int) (eventData.NewValue * (plantMeshes.Count - 1));

        Debug.Log(newIndex);
        ChangePlantMesh(newIndex);

        string res = "";
        foreach (var keys in attributesListDict[newIndex].Keys)
        {
            object val;
            attributesListDict[newIndex].TryGetValue(keys, out val);
            res += (keys.ToString() + ":\t" + val.ToString() + "\n");
            
            // ONly gor MVP Purposes
            if (res.Length > 300)
                break;
        }

        contentTextHandler.ChangeText(res);

        object timestampObj;
        attributesListDict[newIndex].TryGetValue("timestamp", out timestampObj);

        UpdateDateTimeText(timestampObj);
    }

    /// <summary>
    /// Changes the DateTime Text of the Plant Cube
    /// </summary>
    /// <param name="time">Time data to be changed to</param>
    private void UpdateDateTimeText(object time)
    {

        DateTime timeStampDateTime = Convert.ToDateTime(time);
        timestampText.text = (timeStampDateTime.ToString(timeFormatSpecifier, timeFormatCulture));
    }

    /// <summary>
    /// Changes the plant mesh based on the index
    /// </summary>
    /// <param name="index">index of plant mesh in plantMeshes array to be loaded</param>
    public void ChangePlantMesh(int index)
    {
        Mesh newMesh;
        try
        {
            newMesh = (Mesh) plantMeshes[index];
        }
        catch (Exception _)
        {
            Debug.Log("INDEX OUT OF BOUNDS - CANNOT CHANGE PLANT MESH");
            return;
        }

        currentMeshIndex = index;
        ChangePlantMesh(newMesh);
    }

    /// <summary>
    /// Changes the plant mesh based on a given mesh
    /// </summary>
    /// <param name="mesh">Mesh to change to</param>
    public void ChangePlantMesh(Mesh newMesh)
    {
        Debug.Log("Changing Plant Mesh");
        plantMeshFilter.sharedMesh = newMesh;
        CentrePlantMesh();
    }

    [ContextMenu("Centre Plant Mesh")]
    public void CentrePlantMesh()
    {
        // Normalise the scaling of the mesh
        Vector3 size = plantMeshRenderer.bounds.size;
        Vector3 scale = plantMeshRenderer.transform.localScale;

        if (size.x > size.y)
        {
            if (size.x > size.z)
                scale /= size.x;
            else
                scale /= size.z;
        }
        else if (size.z > size.y)
        {
            if (size.z > size.x)
                scale /= size.z;
            else
                scale /= size.x;
        }
        else
            scale /= size.y;

        plantMeshRenderer.transform.localScale = scale/2f;

        // Center the mesh
        Vector3 MeshCentre = plantMeshRenderer.bounds.center;

        plantMeshRenderer.transform.Translate(-MeshCentre, Space.World);

        Vector3 parentPosition = plantMeshRenderer.transform.parent.position;

        plantMeshRenderer.transform.Translate(parentPosition, Space.World);


    }

    /// <summary>
    /// Decrease Plant Mesh Index to the previous plant Mesh 
    /// </summary>
    [ContextMenu("Decrease Plant Mesh")]
    public void DecreasePlantMesh()
    {
        if (currentMeshIndex <= 0)
            return;

        currentMeshIndex--;
        plantMeshFilter.sharedMesh = (Mesh) plantMeshes[currentMeshIndex];
    }

    /// <summary>
    /// Increases Plant Mesh Index to subsequent plant Mesh
    /// </summary>
    [ContextMenu("Increase Plant Mesh")]
    public void IncreasePlantMesh()
    {
        if (currentMeshIndex >= plantMeshes.Count-1)
            return;

        currentMeshIndex++;
        plantMeshFilter.sharedMesh = (Mesh) plantMeshes[currentMeshIndex];
    }

    /// <summary>
    /// Updates the Plant Mesh list for a given Plant block
    /// </summary>
    /// <param name="newPlantMesh">The Mesh of the new Pointcloud to be appended to the list</param>
    /// <param name="assData">The associated data (JSON) from the Online instance of the plant data</param>
    public void UpdatePlantMeshList(Mesh newPlantMesh, PointcloudJSON assData)
    {
        plantMeshes.Add(newPlantMesh);
        plantDataList.Add(assData);

        //sliderForMeshes.SliderStepDivisions = plantMeshes.Count;

        ChangePlantMesh(0);
        CentrePlantMesh();

        titleText.text = assData.dataset_name;

        if (plantMeshes.Count > 1)
        {
            sliderForMeshes.gameObject.SetActive(true);

        }
    }

    public void UpdateSliderStepDivision(int n)
    {
        sliderForMeshes.SliderStepDivisions = n;
    }

    public void ShowError()
    {
        ErrorText.gameObject.SetActive(true);
    }

    public void UpdateMaterial(Material m)
    {
        plantMeshRenderer.GetComponent<MeshRenderer>().material = m;
    }
}
