﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ContentTextHandler : MonoBehaviour
{

    [SerializeField] TextMeshPro tmp;

    public void ChangeText(string text)
    {
        tmp.text = text;
    }
}
