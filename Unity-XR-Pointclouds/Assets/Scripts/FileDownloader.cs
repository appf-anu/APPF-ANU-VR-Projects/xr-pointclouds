﻿using Dummiesman;
using Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization;
using Pcx;
using System;
using System.Collections;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Responsible for Downloading the .ply file and converting it into a Unity Mesh
/// </summary>
public class FileDownloader : MonoBehaviour
{

    //[SerializeField] MeshFilter meshFilter;

    [SerializeField] PlyImporterRuntime plyImporterRuntime;

    [SerializeField] GameObject LoadingIndicatorGO;
    [SerializeField] PlantMeshManager plantMeshManager;

    [SerializeField] string testURL;


    /// <summary>
    /// Test function to Download a ply file from the given test URL
    /// </summary>
    [ContextMenu("Start Download")]
    public void DownloadPlyFromTestURL()
    {
        //Debug.Log(testURL);
        //Task task = DownloadFromTraitCaptureTask(testURL);
        StartCoroutine(DownloadFromTraitCapture(testURL));

    }

    /// <summary>
    /// Downloads mesh from a .ply file and changes to mesh filter
    /// </summary>
    /// <param name="url">url of the .ply file to be downloaded</param>
    IEnumerator DownloadFromTraitCapture(string url)
    {
        Debug.Log("DONWLOADING .....");
        Debug.Log("FROM + " + url);

        string filename = url.Substring(url.Length - 10); // + ".ply";
        string filepath = "DownloadedPlyFiles/" + filename;

        if (LoadingIndicatorGO != null)
            LoadingIndicatorGO.SetActive(true);

        string path = Path.Combine(Application.persistentDataPath, filepath);

        Debug.Log("Initiating UWR downlaod");
        // Web Requests Stuff
        var uwr = new UnityWebRequest(url, UnityWebRequest.kHttpVerbGET);
        uwr.downloadHandler = new DownloadHandlerFile(path);
        yield return uwr.SendWebRequest();
        if (uwr.result != UnityWebRequest.Result.Success)
            Debug.LogError(uwr.error);
        else
            Debug.Log("File successfully downloaded and saved to " + path);

        // Mesh Management Stuff
        Mesh downloadedMesh = plyImporterRuntime.ImportAsMesh(path);
        plantMeshManager.ChangePlantMesh(downloadedMesh);


        Debug.Log("DONE!!!!!!!!!!");
        if (LoadingIndicatorGO != null)
            LoadingIndicatorGO.SetActive(false);

    }

    /// <summary>
    /// Confirms to the Plant Mesh Manager the number of plants at the end of the downloading them all
    /// Useful for changing the step divisions for the stepslider.
    /// </summary>
    /// <param name="numberOfPlants">Number of Plants in this single set</param>
    public void ConfirmNumberOfPlants(int numberOfPlants)
    {
        plantMeshManager.UpdateSliderStepDivision(numberOfPlants);
    }

    public void DownloadPlyFromJSON(PointcloudJSON pc)
    {
        Debug.Log(pc.url);

        Task task = DownloadPointCloudFromJSON(pc);

        //StartCoroutine(DownloadFromTraitCapture(url));
    }


    /// <summary>
    /// Asynchronous Task responsible for downloading data and updating the plant mesh for it
    /// </summary>
    /// <param name="pc">JSON data for the file</param>
    /// <returns>None </returns>
    async Task DownloadPointCloudFromJSON(PointcloudJSON pc)
    {

        //Debug.Log("DONWLOADING FROM API....");
        //Debug.Log("FROM " + pc.url);

        string filename;
        if (pc.is_obj)
            filename = pc.file_name + ".obj";
        else
            filename = pc.file_name + ".ply";

        if (LoadingIndicatorGO != null)
            LoadingIndicatorGO.SetActive(true);

        //string directory = Path.Combine(Application.persistentDataPath, "DownloadedPlyFiles");
        //Directory.CreateDirectory(directory);
        string path = Path.Combine(Application.persistentDataPath, filename);

        HttpClient client = new HttpClient();

        //Debug.Log("Initiating UWR downlaod");
        byte[] responseBody;
        Mesh downloadedMesh;

        // Call asynchronous network methods in a try/catch block to handle exceptions.
        try
        {
            // Only do the download if file does not already exist
            if (!File.Exists(path))
            {
                HttpResponseMessage response = await client.GetAsync(pc.url);
                Debug.Log(response.StatusCode);


                response.EnsureSuccessStatusCode();
                responseBody = await response.Content.ReadAsByteArrayAsync();

                //Debug.Log("Initiating FILE STREAM");
                using (FileStream SourceStream = File.Open(path, FileMode.OpenOrCreate))
                {
                    //Debug.Log("INSIDE USING FILE STREAM");
                    SourceStream.Seek(0, SeekOrigin.End);
                    await SourceStream.WriteAsync(responseBody, 0, responseBody.Length);
                }
            }


            if (pc.is_obj)
            {
                //Debug.Log("TRYING OUT GLTF STUFF");

                byte[] responseBytes = File.ReadAllBytes(path);
                var gltfObject = GltfUtility.GetGltfObjectFromGlb(responseBytes);

                await gltfObject.ConstructAsync();


                //Debug.Log(gltfObject.GameObjectReference.name);
                GameObject gltfMeshObj = gltfObject.GameObjectReference.transform.GetChild(0).gameObject;

                downloadedMesh = gltfMeshObj.GetComponent<MeshFilter>().mesh;
                plantMeshManager.UpdatePlantMeshList(downloadedMesh, pc);

                // add material from gltf file
                Material downloadedMaterial = gltfMeshObj.GetComponent<MeshRenderer>().material;
                plantMeshManager.UpdateMaterial(downloadedMaterial);

                Destroy(gltfObject.GameObjectReference.gameObject);

            }
            else
            {
                downloadedMesh = await plyImporterRuntime.ImportAsMeshAsync(path);
                plantMeshManager.UpdatePlantMeshList(downloadedMesh, pc);
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
            plantMeshManager.ShowError();
        }

        if (LoadingIndicatorGO != null)
            LoadingIndicatorGO.SetActive(false);
    }
}