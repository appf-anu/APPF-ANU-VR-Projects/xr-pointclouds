﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonDownloaderHandler : MonoBehaviour
{

    FileDownloader fileDownloader;
    [SerializeField] GameObject plantCubeToSpawn;

    private PointcloudJSON[] pointcloudDataList;

    [SerializeField] MetadataVisualiserHandler metadataVisHandler;




    public void SetPointCloudDataList(PointcloudJSON[] pointcloudDataList)
    {
        this.pointcloudDataList = pointcloudDataList;
        ChangeMetaData();
    }

    void ChangeMetaData()
    {
        metadataVisHandler.FileCount = pointcloudDataList.Length;
        metadataVisHandler.PointCount = pointcloudDataList[0].point_count;
    }



    public void EnablePlantCube()
    {
        GameObject newPlantCube = Instantiate(plantCubeToSpawn);
        fileDownloader = newPlantCube.GetComponent<FileDownloader>();

        newPlantCube.transform.position = transform.position + Vector3.right*0.75f;

        Debug.Log("Downloading from URL from PLANT CUBE");

        foreach (PointcloudJSON pc in pointcloudDataList)
        {
            fileDownloader.DownloadPlyFromJSON(pc);
        }

        fileDownloader.ConfirmNumberOfPlants(pointcloudDataList.Length-1);

    }

    public void DisablePlantCube()
    {
        plantCubeToSpawn.SetActive(false);
    }
}
