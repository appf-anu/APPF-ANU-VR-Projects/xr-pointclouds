﻿using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that manages the spawning of the Selection Buttons to spawn Plant Cubes from Online
/// </summary>
public class SelectionHandler : MonoBehaviour
{

    [SerializeField] GameObject buttonToSpawn;
    [SerializeField] FileDownloader fileDownloader;
    [SerializeField] JSONManager jsonManager;

    List<GameObject> buttonsGamobjects;

    [SerializeField] int refreshRate = 5;
    int nextTime;

    // Start is called before the first frame update
    void Start()
    {
        UpdateSelectionList();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextTime)
        {
            //Debug.Log("Refreshing at " + Time.time);

            UpdateSelectionList();


            nextTime += refreshRate;
        }
    }

    /// <summary>
    /// Updates selection list to be reflective of those from the Realtime Database
    /// </summary>
    [ContextMenu("Spawn Buttons")]
    public void UpdateSelectionList()
    {
        //Destroy all children
        foreach (Transform child in transform)
            GameObject.Destroy(child.gameObject);
        
        Dictionary<string, PointcloudJSON[]> pointCloudDicts = jsonManager.GetPointCloudURLs();

        float yPos = 0;

        foreach (PointcloudJSON[] pointcloudJsonList in pointCloudDicts.Values)
        {
            GameObject newObject = Instantiate(buttonToSpawn, transform);

            string url = pointcloudJsonList[0].url;

            Vector3 newPos = Vector3.zero;
            newPos.y = yPos;

            newObject.transform.localPosition = newPos;

            yPos -= 0.075f;

            newObject.GetComponent<ButtonConfigHelper>().MainLabelText = pointcloudJsonList[0].dataset_name;

            newObject.GetComponent<ButtonDownloaderHandler>().SetPointCloudDataList(pointcloudJsonList);
        }
    }
}
