﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MetadataVisualiserHandler : MonoBehaviour
{

    [SerializeField] TextMeshPro PointCountField;
    [SerializeField] TextMeshPro FileCountField;



    private int pointCount;
    public int PointCount
    {
        get
        {
            return this.pointCount;
        }
        set
        {
            this.pointCount = value;
            PointCountField.text = "" + value;
        }
    }


    private int fileCount;
    public int FileCount
    {
        get
        {
            return this.fileCount;
        }
        set
        {
            this.fileCount = value;
            FileCountField.text = "" + value;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
