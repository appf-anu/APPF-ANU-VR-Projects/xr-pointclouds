﻿
[System.Serializable]

// CLasses to represent unpacked json from API

public class PointcloudJSON
{
    public string file_name;
    public string url;
    public string dataset_name;
    public string date;
    public int point_count;
    public bool is_obj;
}

public class URLJSON
{
    public string url;
}
public class GraphJSON
{
    public string graphname;
    public int vertexcnt;
    public int edgecnt;
    public GraphJSONVertex[] vertices;
    public GraphJSONEdge[] edges;
    public GraphJSONHighlight highlight;
}

[System.Serializable]
public class GraphJSONVertex
{
    public string name;
    public string description;
    public int idx;
    public float[] pos;

}

[System.Serializable]
public class GraphJSONEdge
{
    public int source;
    public int target;
}

[System.Serializable]
public class GraphJSONHighlight
{
    public int[] nodes;
    public GraphJSONEdge[] edges;
}