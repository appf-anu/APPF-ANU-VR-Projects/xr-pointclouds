﻿using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using TMPro;
using UnityEngine;


/// <summary>
/// Class responsible for the Management of RESTful API requests and retrieval of JSON data
/// </summary>
public class JSONManager : MonoBehaviour
{
    public static List<GraphJSON> graphJSONList;
    public static Dictionary<string, GraphJSON> graphJSONDict = new Dictionary<string, GraphJSON>();

    static readonly HttpClient client = new HttpClient();

    static string API = "https://xr-pointclouds-default-rtdb.asia-southeast1.firebasedatabase.app/";

    [SerializeField] int refreshRate = 5;
    int nextTime;


    [SerializeField] private FileDownloader fileDownloader;

    [SerializeField] TextMeshPro debugText;

    private void Awake()
    {
        nextTime = refreshRate;
    }


    IEnumerator LateStart()
    {

        debugText.text = "Start initiated";
        yield return new WaitForSeconds(2f);

        Login();

    }

    void Start()
    {
        //StartCoroutine(LateStart());
        debugText.text = "Start initiated";

        //Login();
    }

    /// <summary>
    /// Function to obtain PointCloud URLS from the Realtime Database
    /// </summary>
    /// <returns>Dictionary with name as string and Pointclouds as JSON</returns>
    public Dictionary<string, PointcloudJSON[]> GetPointCloudURLs()
    {

        Dictionary<string, PointcloudJSON[]> results = GetJSON<PointcloudJSON[]>(API + "pointclouds.json");
        //Debug.Log("Heres the URL! " + results.get.data.url);
        
        // For debugging purposes
        foreach (PointcloudJSON[] result in results.Values)
        {
            if (result.Length > 1)
            {
                Debug.Log("MULTIPLE ITEMS!!!");
                foreach (PointcloudJSON urlObj in result)
                {
                    Debug.Log("MO " + urlObj.url);
                }
            }
            else
            {
                Debug.Log("SINGLE ITEM!");
                Debug.Log("SO!! " + result[0].url);
            }


        }
        return results;
    }

    [ContextMenu("Print Pointcloud URLs")]
    public void PrintPointcloudURLs()
    {
        Debug.Log(GetPointCloudURLs());
    }


    /// <summary>
    /// Function responsible for fetching URLs from the Realtime Database
    /// </summary>
    /// <returns>List of all URLS</returns>
    public List<string> GetURLs()
    {

        Dictionary<string, URLJSON[]> results = GetJSON<URLJSON[]>(API + "urls.json");
        //Debug.Log("Heres the URL! " + results.get.data.url);

        List<string> res = new List<string>();
        foreach (URLJSON[] result in results.Values)
        {
            if(result.Length > 1)
            {
                Debug.Log("MULTIPLE ITEMS!!!");
                foreach (URLJSON urlObj in result)
                {
                    Debug.Log("MO " + urlObj.url);
                }
            }
            else
            {
                Debug.Log("SINGLE ITEM!");
                Debug.Log("SO!! " + result[0].url);
            }
            //res.Add(result.url);
            //Debug.Log("Heres the URL! " + result.url);

        }
        return res;
    }

    [ContextMenu("Print Multiple URL")]
    public void PrintURLs()
    {
        //Debug.Log(result[0].data.ToString());
        Debug.Log(GetURLs());
    }

    /// <summary>
    /// Function responsible for doing a POST to rhe realtime database
    /// </summary>
    /// <typeparam name="T">Type of the object to serialise the payload into</typeparam>
    /// <param name="url">URL of the API</param>
    /// <param name="obj">payload to post</param>
    /// <returns></returns>
    private static T[] PostJSON<T>(string url, object obj)
    {
        Debug.Log("Preparing API POST ACTION");
        var payload = JsonUtility.ToJson(obj);
        Debug.Log("PAYLOAD CONVERTED TO JSON");
        Debug.Log($"SEND JSON: {payload}");
        var res = client.PostAsync(
            url,
            new StringContent(
                payload,
                System.Text.Encoding.UTF8,
                "application/json"
            )
        ).Result;
        var res2 = res.Content.ReadAsStringAsync().Result;
        Debug.Log($"RETURN JSON: {res2}");

        if (res2.StartsWith("["))
        {
            return JsonConvert.DeserializeObject<T[]>(res2);
        }
        else
        {
            Debug.Log("ERROR " + res2);

            return new T[] { };
        }
    }

    /// <summary>
    /// Performs GET, Retrieves JSON from REST API
    /// </summary>
    /// <typeparam name="T">Type to deserialise retrieved JSON into</typeparam>
    /// <param name="url">URL of the API</param>
    /// <returns>Dictionary containing name as key and the Deserialised JSON object </returns>
    private static Dictionary<string, T> GetJSON<T>(string url)
    {
        var res = client.GetAsync(url).Result;
        var res2 = res.Content.ReadAsStringAsync().Result;

        //res2 = '[' + res2.Substring(1, res2.Length - 2) + ']';
        //res2 = '[' + res2.Substring(1, res2.Length - 2) + ']';
        Debug.Log($"RETURN JSON: {res2}");
        Debug.Log($"RETURN JSONTYPING : " + res2.GetType());

        try
        {
            return JsonConvert.DeserializeObject<Dictionary<string, T>>(res2);

            //return JsonConvert.DeserializeObject<T[]>(res2);
        }
        catch (Exception e)
        {
            Debug.Log("GET JSON ERROR: " + e.Message);
            return new Dictionary<string, T>();
        }
    }



    [Serializable] private class LoginResponse { public string token; }
    [Serializable] private class LoginPayload { public string email; public string pass; }

    public void Login()
    {
        Debug.Log("\n\n LOGGING IN \n\n");
        var user = new LoginPayload();
        user.email = "";
        user.pass = "";


        var res = PostJSON<LoginResponse>(API + "rpc/login", user);
        debugText.text += "\n got payload";

        Debug.Log(user.email);
        if (res.Length > 0)
        {
            string token = res[0].token;
            Debug.Log($"Setting bearer token to {token}");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            Debug.Log("Login Success");

            debugText.text = "Login Success";
        }
        else
        {
            Debug.Log("LOGIN ERROR " + res);


        }
        return;
    }
}

