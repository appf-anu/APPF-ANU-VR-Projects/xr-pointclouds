<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url] -->
<!-- [![MIT License][license-shield]][license-url] -->
<!-- [![LinkedIn][linkedin-shield]][linkedin-url] -->

# XR Pointclouds
Extended Reality Point Cloud Visualisation tool built for the HoloLens.

<!-- PROJECT LOGO -->
<!-- <br />
<p align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">MRTK Custom Gestures in Unity</h3>

  <p align="center">
    Custom Gestures with Mixed Reality Toolkit!
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/septianrazi/MRTK-Custom-Gestures-Unity">View Demo</a>
    ·
    <a href="https://github.com/septianrazi/MRTK-Custom-Gestures-Unity/issues">Report Bug</a>
    ·
    <a href="https://github.com/septianrazi/MRTK-Custom-Gestures-Unity/issues">Request Feature</a>
  </p>
</p> -->



<!-- TABLE OF CONTENTS -->
<!-- <details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#use-in-your-own-projects">Use in your own Projects</a></li>
        <li><a href="#running-the-project">Running this Project</a></li>
      </ul>
    </li>
    <li><a href="#examples">Examples</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details> -->



<!-- ABOUT THE PROJECT -->
## About The Project

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->

This project allows users to visualise Point Cloud Data in Augmented Reality and Virtual Reality, utilising natural interaction methods to explore the 3D structures contextualised with related Attribute MetaData.


### Screenshots

![Main Overview](Screenshots/main_overview.png)

![Manipulation Example GIF](Screenshots/manipulation_example.gif)

#### Also available on Oculus!
![Oculus Example](Screenshots/screenshot_oculus.jpg)




### Built With

* [Unity 2020.1.14](https://unity.com)
* [Microsoft Mixed Reality Toolkit 2.7](https://docs.microsoft.com/en-gb/windows/mixed-reality/mrtk-unity/)
* [Visual Studio 2019](https://visualstudio.microsoft.com/vs/)

## Running the Project

### HoloLens

#### Via Unity
1. Clone the repo
   ```sh
   git clone https://gitlab.com/appf-anu/APPF-ANU-VR-Projects/xr-pointclouds.git
   ```
2. Open Project in Unity (2020 above recommended)
3. Follow the steps [here to build the project into your HoloLens](https://docs.microsoft.com/en-us/windows/mixed-reality/develop/unity/tutorials/mr-learning-base-02?tabs=openxr#building-your-application-to-your-hololens-2)

#### Via App Bundler Sideloading
1. Download the .appx file within the [Builds Folder](https://gitlab.com/appf-anu/APPF-ANU-VR-Projects/xr-pointclouds/-/tree/main/Builds)
2. Follow the steps to [Sideload apps to the Hololens here](https://docs.microsoft.com/en-us/hololens/app-deploy-app-installer)
   

### Oculus

#### Via Unity
1. Clone the repo
   ```sh
   git clone https://gitlab.com/appf-anu/APPF-ANU-VR-Projects/xr-pointclouds.git
   ```
2. Open Project in Unity (2020 above recommended)
3. Follow the steps [here to build the project into your Oculus Device](https://developer.oculus.com/documentation/unity/unity-build/)

#### Via APK Sideloading
1. Download the .apk file within the [Builds Folder](https://gitlab.com/appf-anu/APPF-ANU-VR-Projects/xr-pointclouds/-/tree/main/Builds)
2. Download [Oculus Developer Hub](https://developer.oculus.com/documentation/unity/ts-odh/) and link your Oculus device with it.
3. On My Devices - Apps, Drag and drop the APK file.
   
   
<!-- GETTING STARTED -->
## Contributing

### Prerequisites

You will need to download and install the following:
* [Unity](https://unity3d.com/get-unity/download)

* [Mixed Reality Toolkit for Unity](https://docs.microsoft.com/en-us/windows/mixed-reality/develop/install-the-tools?tabs=unity)

* [Visual Studio](https://visualstudio.microsoft.com/vs/)







<!-- USAGE EXAMPLES -->
<!-- ## Screenshots and Examples

Example Workflow with a basic prototype:

![Example Worflow](Screenshots/ExampleWorkflowMVP.gif) -->


<!-- ROADMAP -->
<!-- ## Roadmap

See the [open issues](https://github.com/othneildrew/Best-README-Template/issues) for a list of proposed features (and known issues). -->



<!-- CONTRIBUTING -->
<!-- ## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Choose an Issue you want to work on and assign yourself

   If there is no exsiting issue, please submit one 

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request linking the issue -->


<!-- LICENSE -->
<!-- ## License

Distributed under the MIT License. See `LICENSE` for more information. -->



<!-- CONTACT -->
## Contact

Septian Razi    - septian.razi@anu.edu.au -  [septianrazi.github.io](septianrazi.github.io) 

Tim Brown       - tim.brown@anu.edu.au

Project Link: [https://gitlab.com/appf-anu/APPF-ANU-VR-Projects/xr-pointclouds](https://gitlab.com/appf-anu/APPF-ANU-VR-Projects/xr-pointclouds)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Australian Plant Phenomics Facility at the Australian National University](https://biology.anu.edu.au/research/facilities/australian-plant-phenomics-facility-anu)
* [OthNeilDrew's README Template](https://github.com/othneildrew/Best-README-Template)
* [Keijiro's Unity PCX reader](https://github.com/keijiro/Pcx)

<!-- 
* [Img Shields](https://shields.io)
* [Choose an Open Source License](https://choosealicense.com)
* [GitHub Pages](https://pages.github.com)
* [Animate.css](https://daneden.github.io/animate.css)
* [Loaders.css](https://connoratherton.com/loaders)
* [Slick Carousel](https://kenwheeler.github.io/slick)
* [Smooth Scroll](https://github.com/cferdinandi/smooth-scroll)
* [Sticky Kit](http://leafo.net/sticky-kit)
* [JVectorMap](http://jvectormap.com)
* [Font Awesome](https://fontawesome.com) -->





<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png
